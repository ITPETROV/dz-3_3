package dz3.part3.task4;

public class Dog {

    private String nameOfDog; // Кличка собаки

    public Dog(int numberDogName) { //
        this.nameOfDog = Participant.dog.get(numberDogName - 1);
    }

    public String getNameOfDog() {
        return nameOfDog;
    }

    public void setNameOfDog(String nameOfDog) {
        this.nameOfDog = nameOfDog;
    }
}
