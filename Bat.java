package dz3.part3.task1;

public class Bat extends Mammal implements Flying {

    @Override
    public void fly() {
        System.out.println("Летает медленно");
    }
}
