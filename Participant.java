package dz3.part3.task4;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Participant {

    int countOfPartisipant;
    static List<String> ownerOfTheDog = new ArrayList<>();
    static List<String> dog = new ArrayList<>();
    static List<Double> score = new ArrayList<>();


    Participant() {
        Scanner scan = new Scanner(System.in);
        countOfPartisipant = scan.nextInt();

        for (int i = 0; i < countOfPartisipant; i++) {
            ownerOfTheDog.add(scan.next());
        }

        for (int i = 0; i < countOfPartisipant; i++) {
            dog.add(scan.next());
        }

        double sumOfScoresDog = 0; // переменная для суммы баллов по каждой собаке
        for (int i = 0; i < countOfPartisipant; i++) {
            sumOfScoresDog = 0; // сумма сбрасывается после каждой собаки
            for (int j = 0; j < 3; j++) {
                sumOfScoresDog += scan.nextDouble();
            }
            score.add(((int) (sumOfScoresDog / 3 * 10)) / 10.0);

        }
    }


    public void getTop() {
        double first = score.get(0);
        int indexFirst = 0;
        // с помощью цикла находим первое место по кол-ву баллов
        for (int i = 1; i < score.size(); i++) {
            if (score.get(i) > first) {
                first = score.get(i);
                indexFirst = i;
            }
        }

        double second = score.get(0);
        int indexSecond = 0;
        // с помощью цикла находим собаку, которая заняла 2-ое место по количеству баллов
        for (int i = 1; i < score.size(); i++) {
            if (score.get(i) < first && score.get(i) > second) {
                second = score.get(i);
                indexSecond = i;
            }
        }

        double third = score.get(0);
        int indexThird = 0;
        for (int i = 1; i < score.size(); i++) {
            if (score.get(i) < first && score.get(i) < second && score.get(i) > third) {
                third = score.get(i);
                indexThird = i;
            }
        }
        System.out.println(ownerOfTheDog.get(indexFirst) + ": " + dog.get(indexFirst) + ", " + score.get(indexFirst));
        System.out.println(ownerOfTheDog.get(indexSecond) + ": " + dog.get(indexSecond) + ", " + score.get(indexSecond));
        System.out.println(ownerOfTheDog.get(indexThird) + ": " + dog.get(indexThird) + ", " + score.get(indexThird));
    }
    
}
