package part1.task2;

public class Student {
    private String name; // Имя студента
    private String surname; // Фамилия студента
    private int[] grades = new int[10]; // Последние 10 оценок студента.

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getSurname() {

        return surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    /**
     * Метод, добавляющий новую оценку в конец массива и удаляющий самую первую оценку(происходит сдвиг массива влево на 1)
     * @param grades Массив, в котором необходимо сделать изменение
     * @param score Оценка, которую необходимо добавить в конец массива
     * @return
     */
    public void setScore(int[] grades, int score) {
        for (int i = 0; i < grades.length - 1; i++) {
            int temp = grades[i + 1];
            grades[i] = temp;

        }
        grades[grades.length - 1] = score;
    }
    public double getAverageStudentScore() {
        double total = 0; // Переменная для суммирования всех оценок массива
        double averageScore = 0; // Переменная для вычисления среднего арифметического от всех оценок в массиве
        for (int i = 0; i < grades.length; i++) {
            total += grades[i];
        }
        return averageScore = ((int)(total / grades.length * 10)) / 10.0;
    }
}
