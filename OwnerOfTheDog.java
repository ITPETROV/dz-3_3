package dz3.part3.task4;

public class OwnerOfTheDog {

    private String nameOfOwner; // имя владельца собаки

    public OwnerOfTheDog(int numberOfOwnerDog) {
        this.nameOfOwner = Participant.ownerOfTheDog.get(numberOfOwnerDog - 1);
    }

    public String getNameOfOwner() {
        return nameOfOwner;
    }

    public void setNameOfOwner(String nameOfOwner) {
        this.nameOfOwner = nameOfOwner;
    }
}
