package dz3.part3.task2;

public class Main2 {
    public static void main(String[] args) {
        Furniture stool = new Stool();
        Furniture table = new Table();
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();

        System.out.println("Сможет ли цех по ремонту мебели починить эту мебель? Ответ: " + bestCarpenterEver.repair(stool)); // Табуретка
        System.out.println("Сможет ли цех по ремонту мебели починить эту мебель? Ответ: " + bestCarpenterEver.repair(table)); // Стол
    }
}
